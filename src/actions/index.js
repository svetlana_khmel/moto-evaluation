import actions from '../constants';
const baseUrl = 'http://localhost:3300/api/merchants/';

const headers = {
  'Accept': 'application.json',
  'Content-Type': 'application/json'
}

export const getData = payload => ({
  type: actions.GET_DATA,
  payload
});

export const chooseBid = (payload) => ({
  type: actions.CHOOSE_BID,
  payload
});

export const setPageOfItems = (payload) => ({
  type: actions.SET_PAGE_OF_ITEMS,
  payload
})

export const loadData = () => {
  return (dispatch) => {
    return fetch(baseUrl, {
      method: 'GET',
      headers
    }).then((response) => {
      return response.json();
    })
    .then((data) => {
      dispatch(getData(data));
      dispatch(setPageOfItems(data))
    })
  }
};

export const editItem = (id) => {
  return (dispatch, getState) => {
    const form = getState()
    const data = form.form.merchantForm.values;

    return fetch(`${baseUrl}${id}`, {
      method: 'PUT',
      headers,
      body: JSON.stringify(data)
    }).then((response) => {
      return response.json();
    })
    .then((data) => {
      dispatch(loadData(data));
    })
  }
};

export const addBidToItem = (id, merchantItem) => {
  return (dispatch) => {
    return fetch(`${baseUrl}${id}`, {
      method: 'PUT',
      headers,
      body: JSON.stringify(merchantItem)
    }).then((response) => {
      return response.json();
    })
    .then(() => {
      dispatch(loadData());
    })
  }
};

export const addItem = () => {
  return (dispatch, getState) => {
    const form = getState()
    const data = form.form.merchantForm.values;

    return fetch(`${baseUrl}`, {
      method: 'POST',
      headers,
      body: JSON.stringify(data)
    }).then((response) => {
      return response.json();
    })
    .then((data) => {
      dispatch(loadData(data));
    })
  }
};

export const deleteItem = (id) => {
  return (dispatch) => {
    if (!id) return;
    return fetch(`${baseUrl}${id}`, {
      method: 'DELETE',
      headers
    }).then((response) => {
      return response.json();
    })
      .then((data) => {
        dispatch(loadData(data));
      })
  }
};
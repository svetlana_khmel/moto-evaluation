import React from 'react';
import { expect } from 'chai';
import App from './App';
import MerchantList from './components/merchant-list';
import { shallow, mount } from 'enzyme';

// import MerchantForm from './containers/merchant-form';
// import BidForm from './containers/bid-form';
// import Pagination from './components/pagination';

describe('App component', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = shallow(<App />);
  })
  it('should render MerchantList', () => {
    expect(wrapper.contains(<MerchantList />)).toEqual(true);
  })
})

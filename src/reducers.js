import actions from './constants';

export default (state = {needUpdate: false}, action) => {
  switch (action.type) {
    case actions.UPDATE_DATA:
      return {...state, ...action.payload}

    case actions.GET_DATA:
      const data = action.payload;
      return {...state, data}

     case actions.CHOOSE_BID:
      const choosenMerchant = action.payload;
      return {...state, choosenMerchant}

    case actions.SET_PAGE_OF_ITEMS:
      const pageOfItems = action.payload;
      return {...state, pageOfItems}
  }

  return state;
};

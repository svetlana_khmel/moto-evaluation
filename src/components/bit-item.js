import React from 'react';
import PropTypes from 'prop-types';

const bidsItem = ({bid}) => {
  this.bid = bid;
  return (<div className={'bid-container'}>
    <div>Bid id: {bid.id}</div>
    <div>Car title: {this.bid.carTitle}</div>
    <div>Car amount: {bid.amount}</div>
    <div>Created: {bid.created}</div>
  </div>)
}
export default bidsItem;

bidsItem.propTypes = {
  bid: PropTypes.object
}



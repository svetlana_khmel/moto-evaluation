const Actions = {
  UPDATE_DATA: 'UPDATE_DATA',
  GET_DATA: 'GET_DATA',
  CHOOSE_BID: 'CHOOSE_BID',
  SET_PAGE_OF_ITEMS: 'SET_PAGE_OF_ITEMS'
}

export default Actions;

import React from 'react';
import { Field, reduxForm } from 'redux-form';
import renderField from '../components/render-field';
import { required } from '../validation';

let MerchantForm = ({handleSubmit}) => {
    return (
      <div className={'merchant-form'}>
        <form onSubmit={handleSubmit}>
          <div>
            <label htmlFor="firstname">First Name</label>
            <Field name="firstname" component={ renderField } type="text" validate={[ required ]} />
          </div>
          <div>
            <label htmlFor="lastname">Last Name</label>
            <Field name="lastname" component={ renderField } type="text" validate={[ required ]} />
          </div>
          <div>
            <label htmlFor="avatarUrl">avatar Url</label>
            <Field name="avatarUrl" component="input" type="text" />
          </div>
          <div>
            <label htmlFor="phone">phone</label>
            <Field name="phone" component="input" type="text" />
          </div>
          <div>
            <label>Has Premium</label>
            <div>
              <label>
                <Field name="hasPremium" component="input" type="radio" value="true" />
                {' '}
                True
              </label>
              <label>
                <Field name="hasPremium" component="input" type="radio" value="false" />
                {' '}
                False
              </label>
            </div>
          </div>
          <div>
            <label htmlFor="email">Email</label>
            <Field name="email" component="input" type="email" />
          </div>
          <button type="submit">Submit</button>
        </form>
      </div>
  );
}

MerchantForm = reduxForm({
  form: 'merchantForm'
})(MerchantForm)

export default MerchantForm